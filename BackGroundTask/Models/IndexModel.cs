﻿using BackGroundTask.Core.IService;
using BackGroundTask.Data.Context;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackGroundTask.Models
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _db;
        private readonly ILogger _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public IndexModel(AppDbContext db, IBackgroundTaskQueue queue,
            ILogger<IndexModel> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _db = db;
            _logger = logger;
            Queue = queue;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public IBackgroundTaskQueue Queue { get; }
    }
}
