﻿using BackGroundTask.Core.IService;
using BackGroundTask.Core.Models;
using BackGroundTask.Data.Context;
using BackGroundTask.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BackGroundTask.Controllers
{
    public class HomeController : Controller
    {
        public IBackgroundTaskQueue Queue { get; }
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IMessageService _messageService;
        private readonly ILogger _logger;

        public HomeController(IBackgroundTaskQueue queue,
            IServiceScopeFactory serviceScopeFactory,
            IMessageService messageService,
            ILogger<HomeController> logger)
        {
            Queue = queue ?? throw new ArgumentNullException(nameof(queue));
            _serviceScopeFactory = serviceScopeFactory;
            _messageService = messageService;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult OnPostAddTaskAsync()
        {
            Queue.QueueBackgroundWorkItem(async token =>
            {
                var guid = Guid.NewGuid().ToString();

                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<AppDbContext>();

                    for (int delayLoop = 1; delayLoop < 4; delayLoop++)
                    {
                        try
                        {
                            db.Messages.Add(
                                new Message()
                                {
                                    Id = Guid.NewGuid(),
                                    Text = $"Queued Background Task { guid } has written a step. { delayLoop }/3"
                                });
                            await db.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, $"An error occurred writing to the database. Error: {ex.Message}");
                        }

                        await Task.Delay(TimeSpan.FromSeconds(5), token);
                    }
                }

                _logger.LogInformation(
                    $"Queued Background Task { guid } is complete. 3/3");
            });

            return RedirectToAction("Index");
        }

        public IActionResult OnPostAddTaskWithOutScopingServiceAsync()
        {
            Queue.QueueBackgroundWorkItem(async token =>
            {
                var guid = Guid.NewGuid().ToString();

                for (int delayLoop = 1; delayLoop < 4; delayLoop++)
                {
                    try
                    {
                        await _messageService.CreateAsync(
                        new Message()
                        {
                            Text = $"Queued Background Task { guid } has written a step. { delayLoop }/3"
                        });
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"An error occurred writing to the database. Error: { ex.Message }");
                    }

                    await Task.Delay(TimeSpan.FromSeconds(5), token);
                }

                _logger.LogInformation($"Queued Background Task { guid } is complete. 3/3");
            });

            return RedirectToAction("Index");
        }
    }
}
