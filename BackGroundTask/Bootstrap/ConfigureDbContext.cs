﻿using BackGroundTask.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackGroundTask.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(IocContainer.Configuration.GetConnectionString("AppDbConnectionKey"));
            });
        }
    }
}
