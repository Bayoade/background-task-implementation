﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace BackGroundTask.Bootstrap
{
    public static class ConfigureLoggingService
    {
        public static void AddLoggingService(this IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory logger)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.File(Path.Combine(env.ContentRootPath, "backgroundtask-log.txt"))
                .CreateLogger();

            logger.AddSerilog();
        }
    }
}
