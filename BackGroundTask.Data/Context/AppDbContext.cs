﻿using BackGroundTask.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace BackGroundTask.Data.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            var messagesModelBuilder = modelBuilder.Entity<Message>().ToTable("Messages");

            messagesModelBuilder.HasIndex(x => x.Id);
        }

        /// This is to make the scope work for a remote accessibility
        public DbSet<Message> Messages { get; set; }

        //internal DbSet<Message> Messages { get; set; }
    }
}
