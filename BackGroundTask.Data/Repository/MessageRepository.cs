﻿using BackGroundTask.Core.IRepository;
using BackGroundTask.Core.Models;
using BackGroundTask.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackGroundTask.Data.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private readonly AppDbContext _dbContext;

        public MessageRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task CreateMessageAsync(Message message)
        {
            _dbContext.Messages.Add(message);
            return _dbContext.SaveChangesAsync();
        }
    }
}
