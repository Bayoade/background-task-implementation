﻿using BackGroundTask.Core.IRepository;
using BackGroundTask.Core.IService;
using BackGroundTask.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackGroundTask.AppService.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;

        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }
        public Task CreateAsync(Message message)
        {
            message.Id = Guid.NewGuid();

            return _messageRepository.CreateMessageAsync(message);
        }
    }
}
