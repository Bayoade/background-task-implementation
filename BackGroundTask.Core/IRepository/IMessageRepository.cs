﻿using BackGroundTask.Core.Models;
using System.Threading.Tasks;

namespace BackGroundTask.Core.IRepository
{
    public interface IMessageRepository
    {
        Task CreateMessageAsync(Message message);
    }
}
