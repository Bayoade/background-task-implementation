﻿using BackGroundTask.Core.Models;
using System.Threading.Tasks;

namespace BackGroundTask.Core.IService
{
    public interface IMessageService
    {
        Task CreateAsync(Message message);
    }
}
