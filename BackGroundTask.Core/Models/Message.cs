﻿using System;

namespace BackGroundTask.Core.Models
{
    public class Message
    {
        public Guid Id { get; set; }

        public string Text { get; set; }
    }
}
